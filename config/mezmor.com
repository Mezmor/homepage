
server {
	listen 80; 
    server_name www.mezmor.com mezmor.com;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/mez/code/homepage;
    }

	location / {
		include     uwsgi_params;
        uwsgi_pass  unix:/home/mez/code/homepage/homepage.sock;
   }
}

