# mezmor.com

#### Config
All system config files are located in config/  
To set them up, might need sudo:
```
cd ~/code/homepage
cp config/mezmor.com /etc/nginx/sites-available/mezmor.com
cp config/homepage.conf /etc/init/homepage.conf
```

`homepage.conf` and `mezmor.com` contain paths that need editing
`wsgi.ini` creates the socket referenced in `mezmor.com`

#### Deploying
1. Make sure all the config files reference the correct paths
2. Start the uwsgi service. Use whatever name you used for the /etc/init/ config
```
sudo start homepage
```
3. Let nginx know the site is enabled
```
cd /etc/nginx/sites-available
cp mezmor.com ../sites-enabled/mezmor.com
```
4. Done. You can always cycle the site with `sudo stop homepage`